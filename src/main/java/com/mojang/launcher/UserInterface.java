package com.mojang.launcher;

import com.mojang.launcher.updater.DownloadProgress;
import com.mojang.launcher.versions.CompleteVersion;

import java.io.File;

public interface UserInterface {

    void showLoginPrompt();

    void setVisible(boolean p0);

    void shutdownLauncher();

    void hideDownloadProgress();

    void setDownloadProgress(DownloadProgress p0);

    void showCrashReport(CompleteVersion p0, File p1, String p2);

    void gameLaunchFailure(String p0);

    void updatePlayState();
}
