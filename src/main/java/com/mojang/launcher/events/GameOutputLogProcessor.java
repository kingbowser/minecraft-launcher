package com.mojang.launcher.events;

import com.mojang.launcher.game.process.GameProcess;

public interface GameOutputLogProcessor {

    void onGameOutput(GameProcess p0, String p1);
}
