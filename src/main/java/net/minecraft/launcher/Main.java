package net.minecraft.launcher;

import com.mojang.launcher.OperatingSystem;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;

public class Main {

    public static void main(final String[] args) {
        final OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();
        final OptionSpec<String> proxyHostOption = parser.accepts("proxyHost").withRequiredArg();
        final OptionSpec<Integer> proxyPortOption = parser.accepts("proxyPort").withRequiredArg().defaultsTo("8080",
                                                                                                             new String[ 0 ]).ofType(
                Integer.class);
        final OptionSpec<File> workDirOption = parser.accepts("workDir").withRequiredArg().ofType(
                File.class).defaultsTo(getWorkingDirectory());
        final OptionSpec<String> nonOption = parser.nonOptions();
        final OptionSet optionSet = parser.parse(args);
        final List<String> leftoverArgs = optionSet.valuesOf(nonOption);
        final String hostName = optionSet.valueOf(proxyHostOption);
        Proxy proxy = Proxy.NO_PROXY;
        if (hostName != null) {
            try {
                proxy = new Proxy(Proxy.Type.SOCKS,
                                  new InetSocketAddress(hostName, optionSet.valueOf(proxyPortOption)));
            } catch (Exception ex) {
            }
        }
        final File workingDirectory = optionSet.valueOf(workDirOption);
        workingDirectory.mkdirs();
        final JFrame frame = new JFrame();
        frame.setTitle("Minecraft Launcher " + LauncherConstants.getVersionName());
        frame.setPreferredSize(new Dimension(900, 580));
        try {
            final InputStream in = Launcher.class.getResourceAsStream("/favicon.png");
            if (in != null) {
                frame.setIconImage(ImageIO.read(in));
            }
        } catch (IOException ex2) {
        }
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        new Launcher(frame, workingDirectory, proxy, null, leftoverArgs.toArray(new String[ leftoverArgs.size() ]),
                     100);
        frame.setLocationRelativeTo(null);
    }

    public static File getWorkingDirectory() {
        final String userHome = System.getProperty("user.home", ".");
        File workingDirectory = null;
        switch (OperatingSystem.getCurrentPlatform()) {
            case LINUX: {
                workingDirectory = new File(userHome, ".minecraft/");
                break;
            }
            case WINDOWS: {
                final String applicationData = System.getenv("APPDATA");
                final String folder = (applicationData != null) ? applicationData : userHome;
                workingDirectory = new File(folder, ".minecraft/");
                break;
            }
            case OSX: {
                workingDirectory = new File(userHome, "Library/Application Support/minecraft");
                break;
            }
            default: {
                workingDirectory = new File(userHome, "minecraft/");
                break;
            }
        }
        return workingDirectory;
    }
}
