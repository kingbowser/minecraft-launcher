package net.minecraft.launcher.updater;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.launcher.OperatingSystem;
import com.mojang.launcher.updater.DateTypeAdapter;
import com.mojang.launcher.updater.LowerCaseEnumTypeAdapterFactory;
import com.mojang.launcher.versions.CompleteVersion;
import com.mojang.launcher.versions.ReleaseType;
import com.mojang.launcher.versions.ReleaseTypeAdapterFactory;
import com.mojang.launcher.versions.Version;
import net.minecraft.launcher.game.MinecraftReleaseType;
import net.minecraft.launcher.game.MinecraftReleaseTypeFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public abstract class VersionList {

    protected final Gson gson;
    private final Map<String, Version> versionsByName;
    private final List<Version> versions;
    private final Map<MinecraftReleaseType, Version> latestVersions;

    public VersionList() {
        this.versionsByName = new HashMap<String, Version>();
        this.versions = new ArrayList<Version>();
        this.latestVersions = Maps.newEnumMap(MinecraftReleaseType.class);
        final GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.registerTypeAdapter(ReleaseType.class,
                                    new ReleaseTypeAdapterFactory(MinecraftReleaseTypeFactory.instance()));
        builder.enableComplexMapKeySerialization();
        builder.setPrettyPrinting();
        this.gson = builder.create();
    }

    public CompleteMinecraftVersion getCompleteVersion(final String name) throws IOException {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be null or empty");
        }
        final Version version = this.getVersion(name);
        if (version == null) {
            throw new IllegalArgumentException("Unknown version - cannot get complete version of null");
        }
        return this.getCompleteVersion(version);
    }

    public Version getVersion(final String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be null or empty");
        }
        return this.versionsByName.get(name);
    }

    public CompleteMinecraftVersion getCompleteVersion(final Version version) throws IOException {
        if (version instanceof CompleteVersion) {
            return (CompleteMinecraftVersion) version;
        }
        if (version == null) {
            throw new IllegalArgumentException("Version cannot be null");
        }
        final CompleteMinecraftVersion complete = this.gson.fromJson(
                this.getContent("versions/" + version.getId() + "/" + version.getId() + ".json"),
                CompleteMinecraftVersion.class);
        final MinecraftReleaseType type = (MinecraftReleaseType) version.getType();
        Collections.replaceAll(this.versions, version, complete);
        this.versionsByName.put(version.getId(), complete);
        if (this.latestVersions.get(type) == version) {
            this.latestVersions.put(type, complete);
        }
        return complete;
    }

    public abstract String getContent(final String p0) throws IOException;

    public void refreshVersions() throws IOException {
        this.clearCache();
        final RawVersionList versionList = this.gson.fromJson(this.getContent("versions/versions.json"),
                                                              RawVersionList.class);
        for (final Version version : versionList.getVersions()) {
            this.versions.add(version);
            this.versionsByName.put(version.getId(), version);
        }
        for (final MinecraftReleaseType type : MinecraftReleaseType.values()) {
            this.latestVersions.put(type, this.versionsByName.get(versionList.getLatestVersions().get(type)));
        }
    }

    protected void clearCache() {
        this.versionsByName.clear();
        this.versions.clear();
        this.latestVersions.clear();
    }

    public CompleteVersion addVersion(final CompleteVersion version) {
        if (version.getId() == null) {
            throw new IllegalArgumentException("Cannot add blank version");
        }
        if (this.getVersion(version.getId()) != null) {
            throw new IllegalArgumentException("Version '" + version.getId() + "' is already tracked");
        }
        this.versions.add(version);
        this.versionsByName.put(version.getId(), version);
        return version;
    }

    public void removeVersion(final String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be null or empty");
        }
        final Version version = this.getVersion(name);
        if (version == null) {
            throw new IllegalArgumentException("Unknown version - cannot remove null");
        }
        this.removeVersion(version);
    }

    public void removeVersion(final Version version) {
        if (version == null) {
            throw new IllegalArgumentException("Cannot remove null version");
        }
        this.versions.remove(version);
        this.versionsByName.remove(version.getId());
        for (final MinecraftReleaseType type : MinecraftReleaseType.values()) {
            if (this.getLatestVersion(type) == version) {
                this.latestVersions.remove(type);
            }
        }
    }

    public Version getLatestVersion(final MinecraftReleaseType type) {
        if (type == null) {
            throw new IllegalArgumentException("Type cannot be null");
        }
        return this.latestVersions.get(type);
    }

    public void setLatestVersion(final String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be null or empty");
        }
        final Version version = this.getVersion(name);
        if (version == null) {
            throw new IllegalArgumentException("Unknown version - cannot set latest version to null");
        }
        this.setLatestVersion(version);
    }

    public void setLatestVersion(final Version version) {
        if (version == null) {
            throw new IllegalArgumentException("Cannot set latest version to null");
        }
        this.latestVersions.put((MinecraftReleaseType) version.getType(), version);
    }

    public String serializeVersionList() {
        final RawVersionList list = new RawVersionList();
        for (final MinecraftReleaseType type : MinecraftReleaseType.values()) {
            final Version latest = this.getLatestVersion(type);
            if (latest != null) {
                list.getLatestVersions().put(type, latest.getId());
            }
        }
        for (final Version version : this.getVersions()) {
            PartialVersion partial = null;
            if (version instanceof PartialVersion) {
                partial = (PartialVersion) version;
            } else {
                partial = new PartialVersion(version);
            }
            list.getVersions().add(partial);
        }
        return this.gson.toJson(list);
    }

    public Collection<Version> getVersions() {
        return this.versions;
    }

    public String serializeVersion(final CompleteVersion version) {
        if (version == null) {
            throw new IllegalArgumentException("Cannot serialize null!");
        }
        return this.gson.toJson(version);
    }

    public abstract boolean hasAllFiles(final CompleteMinecraftVersion p0, final OperatingSystem p1);

    public abstract URL getUrl(final String p0) throws MalformedURLException;

    public void uninstallVersion(final Version version) {
        this.removeVersion(version);
    }

    private static class RawVersionList {

        private List<PartialVersion> versions;
        private Map<MinecraftReleaseType, String> latest;

        private RawVersionList() {
            this.versions = new ArrayList<PartialVersion>();
            this.latest = Maps.newEnumMap(MinecraftReleaseType.class);
        }

        public List<PartialVersion> getVersions() {
            return this.versions;
        }

        public Map<MinecraftReleaseType, String> getLatestVersions() {
            return this.latest;
        }
    }
}
