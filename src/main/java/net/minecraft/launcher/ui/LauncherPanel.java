package net.minecraft.launcher.ui;

import com.mojang.launcher.OperatingSystem;
import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.LauncherConstants;
import net.minecraft.launcher.ui.tabs.LauncherTabPanel;
import org.apache.commons.lang3.SystemUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LauncherPanel extends JPanel {

    public static final String CARD_DIRT_BACKGROUND = "loading";
    public static final String CARD_LOGIN = "login";
    public static final String CARD_LAUNCHER = "launcher";
    private final CardLayout cardLayout;
    private final LauncherTabPanel tabPanel;
    private final BottomBarPanel bottomBar;
    private final JProgressBar progressBar;
    private final Launcher minecraftLauncher;
    private final JPanel loginPanel;
    private JLabel warningLabel;

    public LauncherPanel(final Launcher minecraftLauncher) {
        this.minecraftLauncher = minecraftLauncher;
        this.setLayout(this.cardLayout = new CardLayout());
        this.progressBar = new JProgressBar();
        this.bottomBar = new BottomBarPanel(minecraftLauncher);
        this.tabPanel = new LauncherTabPanel(minecraftLauncher);
        this.loginPanel = new TexturedPanel("/dirt.png");
        this.createInterface();
    }

    protected void createInterface() {
        this.add(this.createLauncherInterface(), "launcher");
        this.add(this.createDirtInterface(), "loading");
        this.add(this.createLoginInterface(), "login");
    }

    protected JPanel createLauncherInterface() {
        final JPanel result = new JPanel(new BorderLayout());
        this.tabPanel.getBlog().setPage("http://mcupdate.tumblr.com");
        if (this.getMinecraftLauncher().getBootstrapVersion() < 100 &&
            OperatingSystem.getCurrentPlatform() == OperatingSystem.WINDOWS && !SystemUtils.IS_JAVA_1_8) {
            (this.warningLabel = new JLabel()).setText(
                    "<html><p style='font-size: 1.1em'>You are running on an old version of Java. Please consider <a " +
                    "href='https://launcher.mojang.com/download/MinecraftInstaller.msi'>using the new Minecraft " +
                    "installer</a> which doesn't require Java, as it may make your game faster.</p></html>");
            this.warningLabel.setForeground(Color.RED);
            this.warningLabel.setHorizontalAlignment(0);
            result.add(this.warningLabel, "North");
            result.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    OperatingSystem.openLink(LauncherConstants.URL_UPGRADE);
                }
            });
        }
        final JPanel center = new JPanel();
        center.setLayout(new BorderLayout());
        center.add(this.tabPanel, "Center");
        center.add(this.progressBar, "South");
        this.progressBar.setVisible(false);
        this.progressBar.setMinimum(0);
        this.progressBar.setMaximum(100);
        this.progressBar.setStringPainted(true);
        result.add(center, "Center");
        result.add(this.bottomBar, "South");
        return result;
    }

    protected JPanel createDirtInterface() {
        return new TexturedPanel("/dirt.png");
    }

    protected JPanel createLoginInterface() {
        this.loginPanel.setLayout(new GridBagLayout());
        return this.loginPanel;
    }

    public Launcher getMinecraftLauncher() {
        return this.minecraftLauncher;
    }

    public LauncherTabPanel getTabPanel() {
        return this.tabPanel;
    }

    public BottomBarPanel getBottomBar() {
        return this.bottomBar;
    }

    public JProgressBar getProgressBar() {
        return this.progressBar;
    }

    public void setCard(final String card, final JPanel additional) {
        if (card.equals("login")) {
            this.loginPanel.removeAll();
            this.loginPanel.add(additional);
        }
        this.cardLayout.show(this, card);
    }
}
