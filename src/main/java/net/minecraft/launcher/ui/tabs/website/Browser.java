package net.minecraft.launcher.ui.tabs.website;

import java.awt.*;

public interface Browser {

    void loadUrl(String p0);

    Component getComponent();

    void resize(Dimension p0);
}
